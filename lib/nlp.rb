
def load_corpus(path)
  corpus = {}
  Dir.new(path).each_child do |f|
    corpus[f] = File.read(File.join(path, f))
  end
  corpus
end

def corpus_frequencies(corpus_dir, n = 3)
  corpus = load_corpus(corpus_dir)
  corpus_freq = {}
  corpus.each { |k, v| corpus_freq[k] = count_frequency(ngrams_of(v, n)) }
  corpus_freq
end

def ngrams_of(sequence, n)
  return [sequence] unless sequence.size > n
  a = []
  i = 0
  while i < sequence.size - n + 1
    a.push(sequence[i, n])
    i += 1
  end
  a
end

def count_frequency(arr)
  h = {}
  arr.each do |e|
    h[e] = 0 unless h.has_key?(e)
    h[e] += 1
  end
  h
end

def intersection_of(a, b)
  h = {}
  a.each do |k, v|
    h[k] = [v, b[k]].min if b.has_key? k
  end
  h
end

def union_of(a, b)
  a.merge(b) { |_, v1, v2| [v1, v2].max }
end

def jaccard(a, b)
  i = intersection_of(a, b).size
  u = union_of(a, b).size
  i.to_f / u
end

def weighted_jaccard(a, b)
  i = intersection_of(a, b).values.sum
  u = union_of(a, b).values.sum
  i.to_f / u
end

def knn_of(file, corpus_freq = CF, n = 3)
  text_freq = count_frequency(ngrams_of(File.read(file), n))
  knn = {}
  corpus_freq.each { |k, v| knn[k] = jaccard(text_freq, v) }
  knn.to_a.sort_by { |_, v| v }.last.first
end

def weighted_knn_of(file, corpus_freq = CF, n = 3)
  text_freq = count_frequency(ngrams_of(File.read(file), n))
  knn = {}
  corpus_freq.each { |k, v| knn[k] = weighted_jaccard(text_freq, v) }
  knn.to_a.sort_by { |_, v| v }.last.first
end

CF = corpus_frequencies(File.join(__dir__,'..','corpus'))
